  paypal.Buttons({
    createOrder: function(data, actions) {
      // Get the selected donation amount from radio buttons
      const selectedAmount = document.querySelector('input[name="donationAmount"]:checked').value;

      // Create the order with the selected amount
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: selectedAmount
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // Capture the payment when the user approves the payment
      return actions.order.capture().then(function(details) {
        // Redirect to thankyou.html on success
        window.location.href = 'thankyou.html';
      });
    },
    onError: function(err) {
      // Redirect to error.html on error
      window.location.href = 'error.html';
    }
  }).render('#paypal-button-container');

